#include <stdio.h>
#include <float.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <ctype.h>
#include <stdlib.h>




void get_size(char * &str_ptr, int size);

int main(void)
{
    char *str_ptr;
    int size = 100;
    get_size(str_ptr, size);
    fgets(str_ptr, size, stdin);
    printf("%s", str_ptr);
    free(str_ptr);
    return 0;
}

void get_size(char * &str_ptr, int size)
{
    str_ptr = (char *)malloc(size);
}
    