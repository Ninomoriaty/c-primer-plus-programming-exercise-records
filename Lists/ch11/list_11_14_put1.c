/* put1.c -- prints a string without adding \n */
#include <stdio.h>
void put1(const char * string) /* string not altered */
{
    while (*string != '\0')
        putchar(*string++);
}

// Alterative
void put1_arr(const char * string) /* string not altered */
{
    int i = 0;
    while (string[i] != '\0')
        putchar(string[i++]);
}

void put1_arr_for(const char * string) /* string not altered */
{
    
    for (int i = 0; string[i] != '\0'; i++)
        putchar(string[i]);
}

void put1_narrow(const char * string) /* string not altered */
{
    while (*string)
        putchar(*string++);
}