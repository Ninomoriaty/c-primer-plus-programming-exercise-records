// addresses.c -- addresses of strings
#define MSG "I'm special." // Tips: if the MSG contain the same string as the argument of printf statement, their addresses will be the same too.
#include <stdio.h>
int main()
{
    char ar[] = MSG;
    const char *pt = MSG;
    printf("address of \"I'm special\": %p \n", "I'm special");
    printf("    address ar: %p\n", ar);
    printf("    address pt: %p\n", pt);
    printf("    address of MSG: %p\n", MSG);
    printf("address of \"I'm special\": %p \n", "I'm special");
    return 0; 
}
