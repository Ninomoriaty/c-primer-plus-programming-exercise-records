#include <stdio.h>
#include <float.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <ctype.h>

void copy_arr(double * arr, double * source, int length);
void copy_ptr(double * arr, double * source, int length);
void copy_ptrs(double * arr, double * source, double * end);
void display_1D_arr(double * arr, int n);

void main(void)
{
    // Variables
    double source[5] = {1.1, 2.2, 3.3, 4.4, 5.5};
    double target1[5];
    double target2[5];
    double target3[5];
    
    // Copy
    copy_arr(target1, source, 5);
    copy_ptr(target2, source, 5);
    copy_ptrs(target3, source, source + 5);

    // Display
    display_1D_arr(source, 5);
    display_1D_arr(target1, 5);
    display_1D_arr(target2, 5);
    display_1D_arr(target3, 5);

}

void copy_arr(double * arr, double * source, int length)
{
    for (int i = 0; i < length; i++)
        arr[i] = source[i];
}

void copy_ptr(double * arr, double * source, int length)
{
    for (int i = 0; i < length; i++)
        *(arr + i) = *(source + i);
}

void copy_ptrs(double * arr, double * source, double * end)
{
    for (int i = 0; (arr + i) < end; i++)
        *(arr + i) = *(source + i);
}


void display_1D_arr(double * arr, int n)
{
    
    printf("{");
    
    for (int i = 0; i < n; i++) {
        printf("%f", arr[i]);
        if (i != n - 1){
            printf(", ");
        }
    }
    
    printf("}\n");
}