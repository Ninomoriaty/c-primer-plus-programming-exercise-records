#include <stdio.h>
#include <float.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <ctype.h>

void reverse_arr(double * arr, int length);
void display_arr(double arr[], int n);

void main(void)
{
    double arr[9] = {120, 13, 2, 0, -1, 2, 3, 4, 19};
    reverse_arr(arr, 9);
    display_arr(arr, 9);
}

void reverse_arr(double * arr, int length)
{
    double temp;
    for (int i = 0; i < length / 2; i++)
    {
        temp = arr[i];
        arr[i] = arr[length - i - 1];
        arr[length - i - 1]= temp;
    }
}

void display_1D_arr(double * arr, int n) {
    
    printf("{");
    
    for (int i = 0; i < n; i++) {
        printf("%f", arr[i]);
        if (i != n - 1){
            printf(" ");
        }
    }
    
    printf("}\n");
}