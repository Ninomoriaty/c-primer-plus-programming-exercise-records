#include <stdio.h>
#include <float.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <ctype.h>

void add_arr(double *arr1, double *arr2, double *result, int length);
void display_1D_arr(double * arr, int n);

int main(void)
{
    // Variables
    double arr1[3] = {1.1, 2.2, 3.3};
    double arr2[3] = {4.4, 5.5, 6.6};
    double result[3];
    
    // Add
    add_arr(arr1, arr2, result, 3);

    // Display
    display_1D_arr(result, 3);

    return 0;

}

void add_arr(double *arr1, double *arr2, double *result, int length)
{
    for (int i = 0; i < length; i++)
    {
        result[i] = arr1[i] + arr2[i];
    }
}

void display_1D_arr(double * arr, int n)
{
    
    printf("{");
    
    for (int i = 0; i < n; i++) {
        printf("%f", arr[i]);
        if (i != n - 1){
            printf(", ");
        }
    }
    
    printf("}\n");
}