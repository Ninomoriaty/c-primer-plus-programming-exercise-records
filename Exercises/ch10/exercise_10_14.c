#include <stdio.h>
#include <float.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <ctype.h>
#include <stdlib.h>




void read_arr(int col, double (*arr)[col])
{
    printf("Please Enter the elements of the array:\n");
    double element;
    int col_num = 0, row_num = 0;
    while (scanf("%lf", &element) == 1 && element != EOF)
    {
        if (col_num >= col)
        {
            row_num++;
            col_num = 0;
        }
        arr[row_num][col_num] = element;
        col_num++;
    }
}

double row_average(int cols, double *arr)
{
    int subtot, i;
    for (i = 0, subtot = 0; i < cols; i++)
        subtot += arr[i];
    return (double) subtot/cols;
}

void show_row_average(int rows, int cols, double arr[rows][cols])
{
    printf("Row  Average\n");
    for (int row = 0; row < rows; row++)
        printf("%3d %5.2f.\n", row, row_average(cols, arr[row]));
}

double total_average(int rows, int cols, double arr[rows][cols])
{
    double total = 0, total_average_value;
    for (int row = 0; row < rows; row++)
        total += cols * row_average(cols, arr[row]);
    total_average_value = total / (rows * cols);
    return total_average_value;
}

double max_arr_element(int rows, int cols, double arr[rows][cols])
{
    double max = 0;
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            if (max < arr[i][j])
                max = arr[i][j];
        }
    }
    return max;
}

void display_2D_arr(int rows, int cols, double arr[rows][cols])
{
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            printf("%5.2f", arr[i][j]);
        }
        printf("\n");
    }
}

void main(void)
{
    // Static initialization
    double arr[3][5];

    // Read and check the input
    read_arr(5, arr);
    display_2D_arr(3, 5, arr);

    // Report
    show_row_average(3, 5, arr);
    printf("Total average: %5.2f.\n", total_average(3, 5, arr));
    printf("Max value of the array: %5.2f.\n", max_arr_element(3, 5, arr));
}
