#include <stdio.h>
#include <float.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <ctype.h>

void slice_copy_arr(double * arr, double * source, int start, int end, int length);
void display_1D_arr(double * arr, int n);

int main(void)
{
    // Variables
    double source[7] = {1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7};
    double target1[3];
    
    // Copy
    slice_copy_arr(target1, source, 2, 4, 7);

    // Display
    display_1D_arr(source, 7);
    display_1D_arr(target1, 3);

    return 0;

}

void slice_copy_arr(double * arr, double * source, int start, int end, int length)
{
    if (start > 0 && start < end && end <= length)
    {
        for (int i = start, i_t = 0; i <= end; i++, i_t++)
            arr[i_t] = source[i];
    }
}

void display_1D_arr(double * arr, int n)
{
    
    printf("{");
    
    for (int i = 0; i < n; i++) {
        printf("%f", arr[i]);
        if (i != n - 1){
            printf(", ");
        }
    }
    
    printf("}\n");
}