#include <stdio.h>
#include <float.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <ctype.h>

void copy_arr(double *arr, double *source, int length);
void copy_2D_arr(int row, int col, double (*arr)[col], double (*source)[col]);
void display_2D_arr(int row, int col, double arr[row][col]);

void main(void)
{
    const double arr_source[3][5] = 
    {
        {0, 1.1, 2.3, 3.5, 0.1},
        {0, 5.9, 6.1, 7.3, 0.2},
        {1, 9.7, 10.9, 11.1, 0.3}
    };
    
    double arr_target[3][5];
    int row = 3, col = 5;
    copy_2D_arr(row, col, arr_target, arr_source);
    display_2D_arr(row, col, arr_target);

}


void copy_arr(double *arr, double *source, int length)
{
    for (int i = 0; i < length; i++)
        arr[i] = source[i];
}

void copy_2D_arr(int row, int col, double (*arr)[col], double (*source)[col])
{
    for (int i = 0; i < row; i++)
        copy_arr(arr[i], source[i], col);
}


void display_2D_arr(int row, int col, double arr[row][col])
{
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            printf("%5.2f", arr[i][j]);
        }
        printf("\n");
    }
}