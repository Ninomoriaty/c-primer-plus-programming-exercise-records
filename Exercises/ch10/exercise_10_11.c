#include <stdio.h>
#include <float.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <ctype.h>

void double_arr(int row, int col, int (*arr)[col]);
void display_2D_arr(int row, int col, int arr[row][col]);

void main(void)
{
    int arr_source[3][5] = 
    {
        {0, 1, 2, 3, 1},
        {0, 5, 6, 7, 2},
        {1, 9, 10, 11, 3}
    };

    double_arr(3, 5, arr_source);
    display_2D_arr(3, 5, arr_source);
}


void double_arr(int row, int col, int (*arr)[col])
{
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
            arr[i][j] = arr[i][j] * 2; 
    }
}


void display_2D_arr(int row, int col, int arr[row][col])
{
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            printf("%5d", arr[i][j]);
        }
        printf("\n");
    }
}