#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

char *str_chr_i(char * str, char char_c);
char * s_gets_i(char * st, int n);

int main(void)
{
    // Constants
    const int LENGTH = 10;

    // Variables
    char str[LENGTH];
    char char_c;
    char *ret_ptr;
    puts("Input-String: (Newline or NULL to quit)");
    while(s_gets_i(str, LENGTH) && str[0] != '\0')
    {
        puts("Input-Char (Newline to change the string)");
        while ((char_c = getchar()) != '\n')
        {
            ret_ptr = str_chr_i(str, char_c);
            if (ret_ptr)
                printf("Address:%p, Value: %c\n", ret_ptr, *ret_ptr);
            
            // Clear the remaining characters
            while(getchar() != '\n')
                continue;
            
            puts("Input-Char (Newline to change the string)");
        }
        
        puts("Input-String: (Newline or NULL to quit)");
    }

    printf("Done.\n");
    return 0;
}

char *str_chr_i(char * str, char char_c)
{
    // Variables
    char *ptr_ret = NULL;
    int counter;

    // Find the first matched character
    for (counter = 0; counter < strlen(str) && str[counter] != char_c; counter++);
    
    // Test if it reach the end of the string
    if (str[counter] == char_c)
        ptr_ret = &str[counter];
    else
        printf("char Not Found.\n");

    // Return the character pointer
    return ptr_ret;
}

char * s_gets_i(char * st, int n)
{
    char * ret_val;
    
    ret_val = fgets(st, n, stdin);
    if (ret_val)
    {
        
        if (strchr(ret_val, '\n'))  // The length of stdin less than n. '\n' is the terminator, which could be replaced.
            *strchr(ret_val, '\n') = '\0';
        else  // The length of stdin larger than n.
            while (getchar() != '\n')
                continue;
    }

    return ret_val;
}