// Header files
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>

// Prototype
int func_word_counter(const char * str_source);
int fetch_words(char * str_target, const char * str_source, int nth);

// main program
int main(int argc, char* argv[])
{
    // Input Validation
    if (argc < 2)
    {
        printf("ERROR: Strings not found.");
    }
    
    // Single Strings Argument
    else if (argc == 2)
    {
        // Input confirmation
        printf("Single Strings Mode\n");
        printf("Received Input String:\n%s\n", argv[1]);

        // Reverse words order
        int word_count = 0;
        char str_target[100];

        word_count = func_word_counter(argv[1]);

        printf("Reverse Word Order:\n");
        while (word_count > 0)
        {
            
            fetch_words(str_target, argv[1], word_count);
            printf("%s ", str_target);
            word_count--;
        }
        printf("\n");
    }
    
    // Multiple Strings Arguments
    else
    {
        // Input confirmation
        printf("Multiple Strings Mode\n");
        printf("Received Input String:\n");
        for (int i = 1; i < argc; i++)
            printf("%s ", argv[i]);
        printf("\n");

        // Reverse words order
        printf("Reverse Word Order:\n");
        for (int i = 1; i < argc; i++)
            printf("%s ", argv[argc - i]);
        printf("\n");
    }

    return 0;
}

int func_word_counter(const char * str_source)
{
    bool in_word = false;
    char c;
    int word_count = 0;
    int subscript = 0;

    while((c = str_source[subscript])!= '\0')
    {
        // The number of words
        if (!isspace(c) && !in_word)
        {
            in_word = true; // starting a new word
            word_count++;  // count word
        }
        if (isspace(c) && in_word)
            in_word = false; // reached end of word
        subscript++;
    }

    return word_count;
}

int fetch_words(char * str_target, const char * str_source, int nth)
{
    
    // Input Validation
    if (nth < 0)
    {
        printf("WordRankError.");
    }

    // Move to the nth word
    int address_word = 0, word_count = 0;
    bool in_word = false;
    char c;

    while((c = str_source[address_word])!= '\0' && word_count < nth)
    {
        // The number of words
        if (!isspace(c) && !in_word)
        {
            in_word = true; // starting a new word
            word_count++;  // count word
        }
        
        if (isspace(c) && in_word)
            in_word = false; // reached end of word
        
        address_word++;
    }
    address_word--;
    
    // Copy the word
    int address_counter_t = 0;
    for(int address_counter_s = 0; 
        !isspace(str_source[address_word + address_counter_s])
        && str_source[address_word + address_counter_s] != '\0'; 
        address_counter_s++)
    {
        str_target[address_counter_t] = str_source[address_word + address_counter_s];
        address_counter_t++;
    }
    str_target[address_counter_t] = '\0';
    
    return 0;
}