// Header files
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>

// Prototype
double pow_i (double num, int power);

// Main
int main (int argc, char* argv[])
{
    // Input Validation
    if (argc != 3)
    {
        printf("InputError: The number of arguments is wrong.\n");
    }
    else 
    {
        // Input confirmation
        double num = atof(argv[1]);
        int power = atoi(argv[2]);

        printf("Input:\n");
        printf("Double: %5.2f; Power: %5d\n", num, power);
        
        // Show the power law program
        printf("Output: %5.2f\n", pow_i(num, power));
        return 0;
    }
    
}

double pow_i (double num, int power)
{
    double db_ret = 1;
    
    for (int i = 0; i < power; i++)
        db_ret *= num;
    
    return db_ret;
}