#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int slice_space(char * str_target, const char * str_source, int start, int end);

int main(void)
{
    
    const int WORDS_LENGTH = 10;
    const int LENGTH = 100;
    
    char str1[WORDS_LENGTH];
    char str2[LENGTH];
    int start = 0, end = LENGTH;

    printf("Input-String:");
    fgets(str2, LENGTH, stdin);

    printf("Input-Start:");
    scanf("%d", &start);

    printf("Input-End:");
    scanf("%d", &end);
    
    printf("Output:");
    slice_space(str1, str2, start, end);
    puts(str1);

    return 0;
}

int slice_space(char * str_target, const char * str_source, int start, int end)
{ 
    if (start < 0 || end > strlen(str_source))
        printf("RangeError");
    else
    {
        for(int counter = 0; start + counter <= end && !isspace(str_source[start + counter]); counter++)
        {
            str_target[counter] = str_source[start + counter];
        }

    }
    
    return 0;
}