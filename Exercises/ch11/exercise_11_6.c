#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int char_num(char * str, char char_c);
char * s_gets_i(char * st, int n);

int main(void)
{
    // Constants
    const int LENGTH = 10;

    // Variables
    char str[LENGTH];
    char char_c;
    int char_count = 0;
    puts("Input-String: (Newline or NULL to quit)");
    while(s_gets_i(str, LENGTH) && str[0] != '\0')
    {
        puts("Input-Char (Newline to change the string)");
        while ((char_c = getchar()) != '\n')
        {
            char_count = char_num(str, char_c);
            printf("Char:%c, Number: %d\n", char_c, char_count);
            
            // Clear the remaining characters
            while(getchar() != '\n')
                continue;
            
            puts("Input-Char (Newline to change the string)");
        }
        
        puts("Input-String: (Newline or NULL to quit)");
    }

    printf("Done.\n");
    return 0;
}

int char_num(char * str, char char_c)
{
    int char_count_ret = 0;
    for (int i = 0; i < strlen(str); i++)
        if (str[i] == char_c)
            char_count_ret++;
    return char_count_ret;
}

char * s_gets_i(char * st, int n)
{
    char * ret_val;
    
    ret_val = fgets(st, n, stdin);
    if (ret_val)
    {
        
        if (strchr(ret_val, '\n'))  // The length of stdin less than n. '\n' is the terminator, which could be replaced.
            *strchr(ret_val, '\n') = '\0';
        else  // The length of stdin larger than n.
            while (getchar() != '\n')
                continue;
    }

    return ret_val;
}