#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int space_purger(char * str_source);
int str_chr_purger(char * str_source, char ch);
char * s_gets_i(char * st, int n);

int main(void)
{
    // Constants
    const int LENGTH = 10;

    // Variables
    char str_source[LENGTH];
    
    puts("Input-String_source: (Newline or NULL to quit)");
    while(s_gets_i(str_source, LENGTH) && str_source[0] != '\0')
    {
        space_purger(str_source);
        printf("Purge Spaces: %s\n", str_source);
        
        puts("Input-String_source: (Newline or NULL to quit)");
    }

    printf("Done.\n");
    return 0;
}

int space_purger(char * str_source)
{
    while(strchr(str_source, ' '))
        str_chr_purger(str_source, ' ');
    
    return 0;
}

int str_chr_purger(char * str_source, char ch)
{
	int int_length = strlen(str_source);
    char *ptr;
    ptr = strchr(str_source, ch);
    
    for(; ptr < &str_source[int_length]; ptr++)
        *(ptr) = *(ptr + 1);
    
    return 0;
}

char * s_gets_i(char * st, int n)
{
    char * ret_val;
    
    ret_val = fgets(st, n, stdin);
    if (ret_val)
    {
        
        if (strchr(ret_val, '\n'))  // The length of stdin less than n. '\n' is the terminator, which could be replaced.
            *strchr(ret_val, '\n') = '\0';
        else  // The length of stdin larger than n.
            while (getchar() != '\n')
                continue;
    }

    return ret_val;
}