// Header files
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

// Function Prototypes
int menu(void);

char * s_gets_i(char * st, int n);
int read_str(int rows, int cols, char (*str_source)[cols]);
int print_str(int rows, int cols, char (*str_source)[cols]);
int print_ptr_arr(int rows, char* *str_ptr);

int fetch_nth_word(char * str_target,  char * str_source, int nth);

int sort_ASCII_collating_sequence(int rows, int cols, char (*str_source)[cols], char* str_order[rows]);
int sort_by_length(int rows, int cols, char (*str_source)[cols], char* str_order[rows]);
int sort_by_length_of_first_word(int rows, int cols, char (*str_source)[cols], char* str_order[rows]);

// main function
int main(void)
{
    // Variables
    int rows, cols;
    
    // Input
    printf("\nPlease enter the rows and cols:\n");
    scanf("%d %d", &rows, &cols);
    
    char str_source[rows][cols];
    char* str_order[rows];
    printf("Please enter the string list:\n");
    read_str(rows, cols, str_source);

    // Menu
    int choice;
    menu();
    printf("Your choice:");
    while(scanf("%d", &choice)) 
    {
        
        // Input Validation
        if (choice > 5 || choice < 0)
        {
            printf("ChoiceError: The choice is invalid. Please check and try again.\n");
        }
        else
        {
            printf("Result-");
            switch(choice)
            {
                case 1:
                    printf("Print the original list of strings:\n");
                    print_str(rows, cols, str_source);
                    break;
                case 2:
                    printf("Print the strings in ASCII collating sequence:\n");
                    sort_ASCII_collating_sequence(rows, cols, str_source, str_order);
                    print_ptr_arr(rows, str_order);
                    break;
                case 3:
                    printf("Print the strings in order of increasing length:\n");
                    sort_by_length(rows, cols, str_source, str_order);
                    print_ptr_arr(rows, str_order);
                    break;
                case 4:
                    printf("Print the strings in order of the length of the first word in the string:\n");
                    sort_by_length_of_first_word(rows, cols, str_source, str_order);
                    print_ptr_arr(rows, str_order);
                    break;
                case 5:
                    printf("Quit.\n");
                    goto end;
            }
        }
        
        // Resend the menu
        menu();
        printf("Your choice:");
    }
    
    // Mark the end
    end: printf("Quit.\n");
}

// UI
int menu(void)
{
    printf("\nWelcome. Make your choice from below options:\n");
    printf("1. Print the original list of strings\n");
    printf("2. Print the strings in ASCII collating sequence\n");
    printf("3. Print the strings in order of increasing length\n");
    printf("4. Print the strings in order of the length of the first word in the string\n");
    printf("5. Quit.\n");

    return 0;
}

// IO
int read_str(int rows, int cols, char (*str_source)[cols])
{

    while (getchar() != '\n')
        continue;
    
    printf("Input-%d String_source: (Newline or NULL to quit)\n", rows);
    int row = 0;
    while(row < rows && s_gets_i(str_source[row], cols)
        && str_source[row][0] != '\0' && str_source[row][0] != EOF)
    {
        row++;
    }

    return 0;
}

int print_str(int rows, int cols, char (*str_source)[cols])
{
    for (int i = 0; i < rows; i++)
        printf("%s\n", str_source[i]);
    return 0;
}

int print_ptr_arr(int rows, char* *str_ptr)
{
    for (int i = 0; i < rows; i++)
        printf("%s\n", str_ptr[i]);
    return 0;
}

char * s_gets_i(char * st, int n)
{
    char * ret_val;
    
    ret_val = fgets(st, n, stdin);
    if (ret_val)
    {
        
        if (strchr(ret_val, '\n'))  // The length of stdin less than n. '\n' is the terminator, which could be replaced.
            *strchr(ret_val, '\n') = '\0';
        else  // The length of stdin larger than n.
            while (getchar() != '\n')
                continue;
    }

    return ret_val;
}

// Slice or Fetch
int fetch_nth_word(char * str_target,  char * str_source, int nth)
{
    
    // Input Validation
    if (nth < 0)
    {
        printf("WordRankError.");
    }

    // Move to the nth word
    int address_word = 0, word_counter = 0;

    while(address_word < strlen(str_source) && word_counter < nth)
    {
        // Sp case: Get the first single character or word.
        if (address_word == 0 && !isspace(str_source[address_word]))
        {
            if (nth == 1)
                break;
            else
                word_counter++;
        }
        
        // Find the address of the nth words.
        if ((isspace(str_source[address_word]) || address_word == 0) 
            && (!isspace(str_source[address_word + 1])))
            word_counter++;
        address_word++;
    }
    
    // Copy the word
    int address_counter_t = 0;
    for(int address_counter_s = 0; 
        !isspace(str_source[address_word + address_counter_s]) 
        && str_source[address_word + address_counter_s] != '\0'; 
        address_counter_s++)
    {
        str_target[address_counter_t] = str_source[address_word + address_counter_s];
        address_counter_t++;
    }
    str_target[address_counter_t] = '\0';
    
    return 0;
}



// Sorting
int sort_ASCII_collating_sequence(int rows, int cols, char (*str_source)[cols], char* str_order[rows])
{
    // Variables
    char *str_temp;
    
    // Use the array of pointers to point to the str_source
    for (int i = 0; i < rows; i++)
    {
        str_order[i] = str_source[i];
    }
    
    // Exchange the pointers' order
    for (int i = 0; i < rows - 1; i++)
    {
        for (int j = i + 1; j < rows; j++)
            if (strncmp(str_order[i], str_order[j], cols) > 0)
            {
                str_temp = str_order[i];
                str_order[i] = str_order[j];
                str_order[j] = str_temp;
            }
    }

    // Return
    return 0;
}

int sort_by_length(int rows, int cols, char (*str_source)[cols], char* str_order[rows])
{
    // Variables
    char *str_temp;
    
    // Use the array of pointers to point to the str_source
    for (int i = 0; i < rows; i++)
    {
        str_order[i] = str_source[i];
    }
    
    // Exchange the pointers' order
    for (int i = 0; i < rows - 1; i++)
    {
        for (int j = i + 1; j < rows; j++)
            if (strlen(str_order[i]) > strlen(str_order[j]))
            {
                str_temp = str_order[i];
                str_order[i] = str_order[j];
                str_order[j] = str_temp;
            }
    }

    // Return
    return 0;
}

int sort_by_length_of_first_word(int rows, int cols, char (*str_source)[cols], char* str_order[rows])
{
    // Variables
    char *str_temp;
    char str_first_i[cols];
    char str_first_j[cols];
    
    // Use the array of pointers to point to the str_source
    for (int i = 0; i < rows; i++)
    {
        str_order[i] = str_source[i];
    }
    
    // Exchange the pointers' order
    for (int i = 0; i < rows - 1; i++)
    {
        for (int j = i + 1; j < rows; j++)
        {
            fetch_nth_word(str_first_i, str_order[i], 1);
            fetch_nth_word(str_first_j, str_order[j], 1);
            if (strlen(str_first_i) > strlen(str_first_j))
            {
                str_temp = str_order[i];
                str_order[i] = str_order[j];
                str_order[j] = str_temp;
            }
        }
    }

    // Return
    return 0;
}

