// Header files
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>


int main(void)
{
    char c;
    int word_count = 0;
    int upper_char = 0;
    int lower_char = 0;
    int punctuations = 0;
    int digits = 0;

    bool in_word = false;
    
    puts("Input-String: (Newline or NULL to quit)");
    while((c = getchar())!= EOF)
    {
        // The number of words
        if (!isspace(c) && !in_word)
        {
            in_word = true; // starting a new word
            word_count++;  // count word
        }
        if (isspace(c) && in_word)
            in_word = false; // reached end of word
        
        // The number of uppercase letters
        if (isupper(c))
            upper_char++;

        // The number of lowercase letters
        if (islower(c))
            lower_char++;
        
        // The number of punctuation charaacters
        if (ispunct(c))
            punctuations++;
        
        // The number of digits
        if (isdigit(c))
            digits++;
    }

    printf("Results:\n");
    printf("Words: %d\n", word_count);
    printf("Uppercase Letters: %d\n", upper_char);
    printf("Lowercase Letters: %d\n", lower_char);
    printf("Punctuations: %d\n", punctuations);
    printf("Digits: %d\n", digits);

    return 0;
}