#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int slice(char * str_target, const char * str_source, int start, int end);

int main(void)
{
    char str1[10];
    char str2[100];
    int start, end;

    printf("Input-String:");
    fgets(str2, 100, stdin);

    printf("Input-Start:");
    scanf("%d", &start);

    printf("Input-End:");
    scanf("%d", &end);
    
    printf("Output:");
    slice(str1, str2, start, end);
    printf("%s", str1);
}

int slice(char * str_target, const char * str_source, int start, int end)
{
    if (start < 0 || end > strlen(str_source))
        printf("RangeError");
    else
    {
        int counter;
        for(counter = 0; start + counter <= end; counter++)
        {
            str_target[counter] = str_source[start + counter];
        }
        *(str_target + counter) = '\0';
    }
    
    return 0;
}