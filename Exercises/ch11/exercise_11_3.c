#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>

int fetch_words(char * str_target, const char * str_source, int nth);

int main(void)
{
    const int WORDS_LENGTH = 10;
    const int LENGTH = 100;

    char str1[WORDS_LENGTH + 1];
    char str2[LENGTH];
    int nth = 0;

    printf("Input-String:");
    fgets(str2, LENGTH, stdin);
    
    printf("Input-The n^th word:");
    scanf("%d", &nth);

    printf("Output-Words:");
    fetch_words(str1, str2, nth);
    puts(str1);

    return 0;
}

int fetch_words(char * str_target, const char * str_source, int nth)
{
    
    // Input Validation
    if (nth < 0)
    {
        printf("WordRankError.");
    }

    // Move to the nth word
    int address_word = 0, word_count = 0;
    bool in_word = false;
    char c;

    while((c = str_source[address_word])!= '\0' && word_count < nth)
    {
        // The number of words
        if (!isspace(c) && !in_word)
        {
            in_word = true; // starting a new word
            word_count++;  // count word
        }
        
        if (isspace(c) && in_word)
            in_word = false; // reached end of word
        
        address_word++;
    }
    address_word--;
    
    // Copy the word
    int address_counter_t = 0;
    for(int address_counter_s = 0; 
        !isspace(str_source[address_word + address_counter_s])
        && str_source[address_word + address_counter_s] != '\0'; 
        address_counter_s++)
    {
        str_target[address_counter_t] = str_source[address_word + address_counter_s];
        address_counter_t++;
    }
    str_target[address_counter_t] = '\0';
    
    return 0;
}