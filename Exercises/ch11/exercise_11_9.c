#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

char * str_reverse(char * str_source);
char * s_gets_i(char * st, int n);

int main(void)
{
    // Constants
    const int LENGTH = 10;

    // Variables
    char str_source[LENGTH];
    
    puts("Input-String_source: (Newline or NULL to quit)");
    while(s_gets_i(str_source, LENGTH) && str_source[0] != '\0')
    {
        printf("Reverse: %s\n", str_reverse(str_source));
        
        puts("Input-String_source: (Newline or NULL to quit)");
    }

    printf("Done.\n");
    return 0;
}

char * str_reverse(char * str_source)
{
    int int_length = strlen(str_source);
    char char_temp;
    
    for (int i = 0; i < (int_length / 2); i++)
    {
        char_temp = str_source[i];
        str_source[i] = str_source[int_length - i - 1];
        str_source[int_length - i - 1] = char_temp;
    }
        
    return str_source;
}


char * s_gets_i(char * st, int n)
{
    char * ret_val;
    
    ret_val = fgets(st, n, stdin);
    if (ret_val)
    {
        
        if (strchr(ret_val, '\n'))  // The length of stdin less than n. '\n' is the terminator, which could be replaced.
            *strchr(ret_val, '\n') = '\0';
        else  // The length of stdin larger than n.
            while (getchar() != '\n')
                continue;
    }

    return ret_val;
}