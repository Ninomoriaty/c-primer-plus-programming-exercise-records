#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

char * strstr_i(char * str_source, char * str_cmp);
int slice(char * str_target, const char * str_source, int start, int end);
char * s_gets_i(char * st, int n);

int main(void)
{
    // Constants
    const int LENGTH = 10;

    // Variables
    char str_source[LENGTH];
    char str_cmp[LENGTH];
    char *ptr_cmp_in_source;
    
    puts("Input-String_source: (Newline or NULL to quit)");
    while(s_gets_i(str_source, LENGTH) && str_source[0] != '\0')
    {
        puts("Input-String_compare: (Newline to change the string)");
        while (s_gets_i(str_cmp, LENGTH) && str_cmp[0] != '\0')
        {
            if (ptr_cmp_in_source = strstr_i(str_source, str_cmp))
            {
                char matched_value[LENGTH];
                slice(matched_value, ptr_cmp_in_source, 0, strlen(str_cmp));
                printf("Address: %p\nFirst char: %c\nMatched_Value: %s\n", ptr_cmp_in_source, *ptr_cmp_in_source, matched_value);
            }
            else
                printf("%s Not Found.\n", str_cmp);

            
            puts("Input-String_compare: (Newline to change the string)");
        }
        
        puts("Input-String_source: (Newline or NULL to quit)");
    }

    printf("Done.\n");
    return 0;
}

char * strstr_i(char * str_source, char * str_cmp)
{
    int i = 0;
    char * ptr_ret = NULL;
    int found_or_not = 1;
    
    while((found_or_not = strncmp(str_source + i, str_cmp, strlen(str_cmp))) 
        && (i < strlen(str_source) - strlen(str_cmp)))
        i++;
    
    if (found_or_not == 0)
        ptr_ret = str_source + i;
    
    return ptr_ret;
}


int slice(char * str_target, const char * str_source, int start, int end)
{
    if (start < 0 || end > strlen(str_source))
        printf("RangeError");
    else
    {
        int counter;
        for(counter = 0; start + counter < end; counter++)
        {
            *(str_target + counter) = *(str_source + start + counter);
        }
        *(str_target + counter) = '\0';
    }
    
    return 0;
}

char * s_gets_i(char * st, int n)
{
    char * ret_val;
    
    ret_val = fgets(st, n, stdin);
    if (ret_val)
    {
        
        if (strchr(ret_val, '\n'))  // The length of stdin less than n. '\n' is the terminator, which could be replaced.
            *strchr(ret_val, '\n') = '\0';
        else  // The length of stdin larger than n.
            while (getchar() != '\n')
                continue;
    }

    return ret_val;
}