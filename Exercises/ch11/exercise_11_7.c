#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

char * strncpy_i(char * str_target, const char * str_source, int n);
char * s_gets_i(char * st, int n);

int main(void)
{
    // Constants
    const int LENGTH = 100;

    // Variables
    char str_source[LENGTH];
    int copy_length;

    puts("Input-String: (Newline or NULL to quit)");
    while(s_gets_i(str_source, LENGTH) && str_source[0] != '\0')
    {
        puts("Input-Copy length:");
        scanf("%d", &copy_length);
        char str_target[copy_length];
        
        strncpy_i(str_target, str_source, copy_length);
        printf("Source: %s\nTarget: %s\n", str_source, str_target);
    }

    printf("Done.\n");
    return 0;
}

char * strncpy_i(char * str_target, const char * str_source, int n)
{
    int i;
    for (i = 0; i < n; i++)
        *(str_target + i) = *(str_source + i);
    *(str_target + i) = '\0';

    return str_target;
    
}

char * s_gets_i(char * st, int n)
{
    char * ret_val;
    
    ret_val = fgets(st, n, stdin);
    if (ret_val)
    {
        
        if (strchr(ret_val, '\n'))  // The length of stdin less than n. '\n' is the terminator, which could be replaced.
            *strchr(ret_val, '\n') = '\0';
        else  // The length of stdin larger than n.
            while (getchar() != '\n')
                continue;
    }

    return ret_val;
}


