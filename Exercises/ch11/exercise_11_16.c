// Header files
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main(int argc, char* argv[])
{
    
    puts("Input-String:");
    printf("Choice: %s\n", argv[1]);

    int flag = 0;
    if (argc == 1)
    {
        printf("Default option -p.\n");
        flag = 'p';
    }
    else if (argc == 2 && strlen(argv[1]) == 2)
    {
        flag = argv[1][1];
    }
    else
    {
        printf("Error: Invalid Argument.\n");
    }
    
    char c;
    while(flag && (c = getchar()) != EOF)
    {
        switch (flag)
        {
        case 'p':
            putchar(c);
            break;
        case 'u':
            putchar(toupper(c));
            break;
        case 'l':
            putchar(tolower(c));
            break;
        default:
            printf("Warning: Unknown problems.\n");
            break;
        }

    }
    printf("Done.\n");
    return 0;
}