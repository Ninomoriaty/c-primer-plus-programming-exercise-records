// Header files
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int atoi_i (char * str);
char * s_gets_i(char * st, int n);
double pow_i (double num, int power);

int main(void)
{
    const int LENGTH = 10;
    
    char str[LENGTH];
    
    printf("Input-digits:\n");
    while(s_gets_i(str, LENGTH) && str[0] != '\0')
    {
        printf("Output-Result:\n%d\n", atoi_i(str));
        printf("Input-digits:\n");
    }
        
}

int atoi_i (char * str)
{
    int int_ret = 0;

    int length = strlen(str);

    for (int i = 0; i < length ; i++)
    {
        if (isdigit(str[i]))
            int_ret += (str[i] - '0') * (int)pow_i(10, length - i - 1);
        else
        {
            printf("InputError: Non-digit value found.");
            int_ret = 0;
            break;
        }
    }

    return int_ret;
}

char * s_gets_i(char * st, int n)
{
    char * ret_val;
    
    ret_val = fgets(st, n, stdin);
    if (ret_val)
    {
        
        if (strchr(ret_val, '\n'))  // The length of stdin less than n. '\n' is the terminator, which could be replaced.
            *strchr(ret_val, '\n') = '\0';
        else  // The length of stdin larger than n.
            while (getchar() != '\n')
                continue;
    }

    return ret_val;
}

double pow_i (double num, int power)
{
    double db_ret = 1;
    
    for (int i = 0; i < power; i++)
        db_ret *= num;
    
    return db_ret;
}
