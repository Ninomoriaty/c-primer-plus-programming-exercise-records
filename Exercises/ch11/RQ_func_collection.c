#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * s_gets_i(char * st, int n);
int str_len_i(char * str);

int main(void)
{
    char str[40];
    s_gets_i(str, 10);
    printf("Content: %s\nLength: %d\n", str, str_len_i(str));
}

char * s_gets_i(char * st, int n)
{
    char * ret_val;
    
    ret_val = fgets(st, n, stdin);
    if (ret_val)
    {
        
        if (strchr(ret_val, '\n'))  // The length of stdin less than n. '\n' is the terminator, which could be replaced.
            *strchr(ret_val, '\n') = '\0';
        else  // The length of stdin larger than n.
            while (getchar() != '\n')
                continue;
    }

    return ret_val;
}

int str_len_i(char * str)
{
    return strchr(str, '\0') - str;
}

