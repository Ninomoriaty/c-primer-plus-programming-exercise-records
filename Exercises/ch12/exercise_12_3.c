
#include <stdio.h>

int get_show_info(int mode);

int main(void)
{
    int mode = 0, ex_mode = 0;
    printf("Enter 0 for metric mode, 1 for US mode: ");
    scanf("%d", &mode);
    
    while (mode >= 0)
    {
        if (mode == 0 || mode == 1)
            ex_mode = mode;
        else
        {
            printf("Invalid mode %d specified. Mode %d used.\n", mode, ex_mode);
        }

        get_show_info(mode);
        
        printf("Enter 0 for metric mode, 1 for US mode");
        printf(" (-1 to quit): ");
        scanf("%d", &mode);
    }
    
    printf("Done.\n");
    return 0;
}

int get_show_info(int mode)
{
    double distance;
    double fuel;
    switch (mode)
    {
    case 0:
        printf("Enter distance traveled in kilometers: ");
        scanf("%lf", &distance);
        printf("Enter fuel consumed in liters: ");
        scanf("%lf", &fuel);
        printf("Fuel consumption is %5.2f liters per 100 km.\n", fuel/(distance / 100));
        break;
    case 1:
        printf("Enter distance traveled in miles: ");
        scanf("%lf", &distance);
        printf("Enter fuel consumed in gallons: ");
        scanf("%lf", &fuel);
        printf("Fuel consumption is %5.1f miles per gallon.\n", distance / fuel);
        break;
    default:
        printf("Error: Invalid mode option.");
        break;
    }

    return 0;
}
