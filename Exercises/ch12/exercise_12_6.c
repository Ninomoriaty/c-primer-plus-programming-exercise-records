#include <stdio.h>
#include <stdlib.h>
#include <time.h>



int main()
{
    int SIZE = 0;
    int range = 10;
    printf("Input-Size: ");
    scanf("%d", &SIZE);

    while (SIZE > 0)
    {   
        srand((unsigned int) time(0));
        printf("Random %d int in the range 1-10:\n", SIZE);
        
        int * counter;
        counter = (int * ) calloc(range, sizeof(int));

        for (int i = 0; i < SIZE; i++)
            counter[rand() % range]++;
        
        printf("Value Count\n");
        for (int i = 0; i < range; i++)
            printf("%5d %5d\n", i + 1, counter[i]);

        free(counter);
        printf("Input-Size(0 to quit): ");
        scanf("%d", &SIZE);
    }
}