#include <stdio.h>

static int mode;  // default 0
static double distance;
static double fuel;

int set_mode(int ex_mode)
{
    if (ex_mode == 0 || ex_mode == 1)
        mode = ex_mode;
    else
    {
        printf("Invalid mode %d specified. Mode %d used.\n", ex_mode, mode);
    }
    return 0;
}

int get_info()
{
    switch (mode)
    {
    case 0:
        printf("Enter distance traveled in kilometers: ");
        scanf("%lf", &distance);
        printf("Enter fuel consumed in liters: ");
        scanf("%lf", &fuel);
        break;
    case 1:
        printf("Enter distance traveled in miles: ");
        scanf("%lf", &distance);
        printf("Enter fuel consumed in gallons: ");
        scanf("%lf", &fuel);
        break;
    default:
        printf("Error: Invalid mode option.");
        break;
    }

    return 0;
}

int show_info()
{
    switch (mode)
    {
    case 0:
        printf("Fuel consumption is %5.2f liters per 100 km.\n", fuel/(distance / 100));
        break;
    case 1:
        printf("Fuel consumption is %5.1f miles per gallon.\n", distance / fuel);
        break;
    default:
        printf("Error: Invalid mode option.\n");
        break;
    }
    return 0;
}