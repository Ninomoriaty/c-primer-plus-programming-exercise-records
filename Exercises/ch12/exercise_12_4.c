#include <stdio.h>

int counter = 0;

int counter_function();

int main()
{
    int times;
    
    printf("Input-Loop times: ");
    scanf("%d", &times);
    while (counter < times)
    {
        printf("Output-Times: %d\n", counter_function());
    }
    return 0;
}

int counter_function()
{
    extern int counter;
    counter++;
    return counter;
}