#include <stdio.h>
#include <stdlib.h>
#include <time.h>

static int SIZE = 100;
int sort_num(int * list);

int main()
{
    
    int list[SIZE];
    int choice;

    printf("Input-Continue with 1 or Stop with 0:");
    scanf("%d", &choice);
    while (choice == 1)
    {   
        srand((unsigned int) time(0));
        printf("Random %d int in the range 1-10:\n", SIZE);
        int i;
        for (i = 0; i < SIZE; i++)
        {
            list[i] = rand() % 10 + 1;
            printf("%2d ", list[i]);
            if (i % 6 == 5)
                printf("\n");
        }
        if (i % 6 != 0)
            putchar('\n');
        
        printf("Sorting the %d int in the range 1-10:\n", SIZE);
        sort_num(list);
        for (i = 0; i < SIZE; i++)
        {
            printf("%2d ", list[i]);
            if (i % 6 == 5)
                printf("\n");
        }
        if (i % 6 != 0)
            putchar('\n');

        printf("Input-Continue with 1 or Stop with 0:");
        scanf("%d", &choice);
    }
}

int sort_num(int * list)
{
    int temp;
    extern int SIZE;

    for (int i = 0; i < SIZE - 1; i++)
    {
        for (int j = i + 1; j < SIZE; j++)
        {
            if (list[i] < list[j])
            {
                temp = list[i];
                list[i] = list[j];
                list[j] = temp;
            }
        }
    }
    return 0;
}
