// Header files
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>


// Prototypes
char * s_gets_i(char * st, int n);
int print_ptr_arr(int rows, char* *str_ptr);
int fetch_nth_word(char * str_target,  char * str_source, int nth);


// main function
int main(void)
{
    // Constant
    const int WORD_MAX_LEN = 100;

    // Variables
    int rows;
    
    // Input
    printf("How many words do you wish to enter? :\n");
    scanf("%d", &rows);
    while (getchar() != '\n')
        continue;

    char word[WORD_MAX_LEN];
    char str_source[WORD_MAX_LEN * rows];
    char ** str_ptr = (char **) malloc(rows * sizeof(char *));

    
    printf("Enter %d words now:\n", rows);
    s_gets_i(str_source, WORD_MAX_LEN * rows);

    printf("Here are your words:\n");
    // Store the words within a ptr2ptr2char
    for (int i = 0; i < rows; i++) 
    {
        fetch_nth_word(word,  str_source, i + 1);
        str_ptr[i] = (char *) malloc(strlen(word) + 1);
        strncpy(str_ptr[i], word, strlen(word) + 1);
    }

    // Output
    print_ptr_arr(rows, str_ptr);
    free(str_ptr);

    return 0;
}


//IO
char * s_gets_i(char * st, int n)
{
    char * ret_val;
    
    ret_val = fgets(st, n, stdin);
    if (ret_val)
    {
        
        if (strchr(ret_val, '\n'))  // The length of stdin less than n. '\n' is the terminator, which could be replaced.
            *strchr(ret_val, '\n') = '\0';
        else  // The length of stdin larger than n.
            while (getchar() != '\n')
                continue;
    }

    return ret_val;
}

int print_ptr_arr(int rows, char* *str_ptr)
{
    for (int i = 0; i < rows; i++)
    {
        printf("%s\n", str_ptr[i]);
    }
    return 0;
}


// Slice or Fetch
int fetch_nth_word(char * str_target,  char * str_source, int nth)
{
    
    // Input Validation
    if (nth < 0)
    {
        printf("WordRankError.");
    }

    // Move to the nth word
    int address_word = 0, word_counter = 0;

    while(address_word < strlen(str_source) && word_counter < nth)
    {
        // Sp case: Get the first single character or word.
        if (address_word == 0 && !isspace(str_source[address_word]))
        {
            if (nth == 1)
                break;
            else
                word_counter++;
        }
        
        // Find the address of the nth words.
        if ((isspace(str_source[address_word]) || address_word == 0) 
            && (!isspace(str_source[address_word + 1])))
            word_counter++;
        address_word++;
    }
    
    // Copy the word
    int address_counter_t = 0;
    for(int address_counter_s = 0; 
        !isspace(str_source[address_word + address_counter_s]) 
        && str_source[address_word + address_counter_s] != '\0'; 
        address_counter_s++)
    {
        str_target[address_counter_t] = str_source[address_word + address_counter_s];
        address_counter_t++;
    }
    str_target[address_counter_t] = '\0';
    
    return 0;
}


