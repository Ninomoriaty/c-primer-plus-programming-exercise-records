/* manydice.c -- multiple dice rolls */
/* compile with diceroll.c */
#include <stdio.h>
#include <stdlib.h>  /* for library srand() */
#include <time.h>  /* for time() */
#include "diceroll.h"  /* for roll_n_dice() */
/* and for roll_count */

int main(void)
{
    int sets;
    int dice;
    int sides;
    int status;

    srand((unsigned int) time(0));  /* randomize seed */

    printf("Enter the number of sets; enter q to stop: ");
    while (scanf("%d", &sets))
    {
        printf("Enter the number of sides per die and dice(q to stop): ");
        if ((status = scanf("%d %d", &sides, &dice)) != 2 || sides < 0 || dice < 0)
        {
            if (status == EOF)
                break;  /* exit loop */
            else
            {
                printf("You should have entered two integer separated by space.");
                printf(" Let's begin again.\n");

                while (getchar() != '\n')
                    continue;  /* dispose of bad input */
                
                printf("Enter the number of sides per die and dice(q to stop): \n");
                continue;  /* new loop cycle */
            }
        }

        printf("Here are %d sets of %d %d-sided throws.\n", sets, dice, sides);
        int i;
        for (i = 0; i < sets; i++)
        {
            printf("%3d", roll_n_dice(dice, sides));
            if (i % 15 == 14)
                printf("\n");
        }
        if (i % 15 != 0)
            printf("\n");
        
        printf("Enter the number of sets; enter q to stop: ");
    }

    printf("GOOD FORTUNE TO YOU!\n");
    return 0;
}